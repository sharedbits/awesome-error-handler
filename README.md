# Awesome Error Handler for Express

[screenshot of the Stack trace]

Express default error handling is just ok.
The npm module [errorhandler](https://github.com/expressjs/errorhandler) improves it but does not fix it.
Express lets you  [define you own error handlers](https://expressjs.com/en/guide/error-handling.html
)
This module will make error handling *awesome*.

## Intro

Awesome Error Handler wants to give engineers a faster and better development experience and be still usable in production.

## Simple use
``` javascript
const awesomeErrorHandler = require('awesome-error-handler');

const app = express();
// Initialize some middleware helpers
awesomeErrorHandler.initialize({ app });

... Your middlewares and routes here ...

// The error route goes last
app.use(awesomeErrorHandler({ app }));
```

## Features

These are some of the features of the handler

### Catching asynchronous errors and coloring of trace line (with [trycatch](https://www.npmjs.com/package/trycatch))

Currently a lot of node modules still use asynchronous primitive with callbacks.
Unfortunately there is no simple way to intercept those error. 
The standard `try...catch` cannot catch these errors.

```
// Simple example
try { 
  setTimeout(() => throw new Error('Catch me if you can'), 0);
} catch (err) {
  // This will NOT execute!
  console.error('Error:', err);
}
```

In a better future when all modules will use `async/await` and `Promise`, error handling in Javascript will be a lot simpler. But at the moment this is the status quo.

[Trycatch](https://www.npmjs.com/package/trycatch) is doing some magic and wrap all node asyncronous Node APIs so it can catch those errors. I learn to appreciate its monkey patching that save a lot of time in development. 

As extra bonus, [trycatch](https://www.npmjs.com/package/trycatch) colorize the stack trace so it is easy to recognize the code in your repo. And finally it can give you long stack traces that show where the error originated in the chain of asynchronous calls.

Another similar npm module you may be familiar with that does something similar is [longjohn](https://www.npmjs.com/package/longjohn).

### Errors in the browser
When you have an error in a route that is not catch, you will see a page with detail about the HTTP error, stack trace and other data in the request.  

### JSON responses when needed
If the call from a client accepting `application/json`, we want to 

### Do not leak data to the clients in production
In production we want to avoid to return to the client (usually the browser) stack traces or other sensitive information. The default behavior is not to show the detailed error to the user but track everything in the logs.

### Request as curl 
An error will show and a CURL request that can easily be used from command line to replicate the call.

### Highly customizable
Awesome Error Handler try to use good defaults but giving the flexibility necessary for most use cases and customization. 
For example , you may want to use your own logger that use your own format.   

## Advance use







# New NPM module template with ESNext support

This is a good starting to create a middleware that is backward compatible with node 4
but can use the new ESNext syntax with babel.

# What is in the package

- Execution with Babel (env preset) in [.babelrc](./.babelrc). Only compile what is not available. Support for (`import`/`export`).
- Bundling with [rollup](https://rollupjs.org/) in one distribution file (with sourcemaps).
- Building for node 4 with (env preset) in [.babelrc-build](./.babelrc-build) in `/dist`.
- Linting with ESLint (setup your lint in [.eslintrc.js](./.eslintrc.js) - currently set with ebay template).
- Test with [JEST](https://facebook.github.io/jest/docs/en/getting-started.html)
  - Test are local in `.spec.js` files.
  - Coverage test enforced (currently 50%) but you can change it in the [package.json](./package.json).
- Precommit hook runs tests and linting (with `--fix` so you do not waste time).
- Set up node version with nvm in `.nvmrc`.
- `npm publish` run test and build the repo.
-  Step by step debugging with `npm run debugTest` (use with [`chrome://inspect`](https://medium.com/the-node-js-collection/debugging-node-js-with-google-chrome-4965b5f910f4)).
-  Watching tests with `watch:test` (use it while you develop!)
-  Coverage with `npm run cover` and HTML report in the browser with `npm run coverHTML`

# How to use it

- Clone this repo
- `npm install -g yarn`
- `yarn`
- `npm run watch:test` while you develop!
- Modify the file in [./lib](./lib) as you like

# TODO 

- [ ] Fix linting
- [ ] Add basic tests
- [ ] Proof read documentation [README](./README/md) 
- [ ] Refactor `colorize` name
- [ ] Add at least a screenshot
- [ ] Update author/commiter
- [ ] Publish 0.2
- [ ] Install Movie maker
- [ ] Investigate how to make a video in a GIF/PNG? (+ editing )
   - probably a APNG and I can use [ffmpeg](https://stackoverflow.com/questions/43795518/using-ffmpeg-to-create-looping-apng)
   - https://gist.github.com/paulirish/b6cf161009af0708315c ?
- [ ] Make a video
    - Start for demo in DEV
    - Show divide error
    - Show HTML
    - Interact with HTML
        - Clean Trace
        - Show headers
        -  Sticky
    - Show stack trace in console
    - Navigate to a file in MS code
    - Repeat in production mode
- Submit PR to https://github.com/sindresorhus/awesome-nodejs
- Investigate features of [react-overlay](https://github.com/facebook/create-react-app/tree/next/packages/react-error-overlay) (see also https://github.com/storybooks/storybook/pull/2900)
- [ ] Add other examples
  - [ ] Different logger
  - [ ] Short tracktrace
  - [ ] Production
  - [ ] Dev
  - [ ] Example of turning on trycatch (on running) 
- [ ] Check: https://github.com/googlearchive/stacky 
- [ ] Separate Options pan from the others
- [ ] Editing option live
- [ ] Get feedback from designer
- [ ] Get feedback from engineers
- [ ] Add styling on action (not only text)
- [ ] Refactor how we save and update the dom

# Other art and resources

### Error handlers
- https://github.com/expressjs/errorhandler
- https://gist.github.com/zcaceres/2854ef613751563a3b506fabce4501fd

### Async error catching and stack
- https://www.npmjs.com/package/trycatch
- https://www.npmjs.com/package/nn-node-stacktrace
- https://github.com/mvaldesdeleon/long-promise
- https://www.npmjs.com/package/erotic !!!
- https://www.npmjs.com/package/async-stacktrace
- https://github.com/olstenlarck/clean-stacktrace
- https://www.npmjs.com/package/deepstack

### Stack formatting
https://github.com/AriaMinaei/pretty-error
https://github.com/googlearchive/stacky
https://www.npmjs.com/package/stack
https://github.com/shinnn/neat-stack

### Frontend stack capturing
https://github.com/stacktracejs/stacktrace.js/

### Awesome nodejs
https://github.com/sindresorhus/awesome-nodejs#debugging--profiling
https://github.com/valyouw/njstrace
https://github.com/watson/stackman

const escapeHTML = require('./escapeHTML').escapeHTML;

const defaultColorMap = ['node', 'default', 'modules', '', ''];
// eslint-disable-next-line no-control-regex
const regEx = new RegExp('\x1b\\[([0-9]{1,2})m(.*)\x1b\\[[0-9]{2}m.*\n', 'g');
const extraNodeRegEx = new RegExp('\\(internal');

const V8_OUTER1 = /^\s*(eval )?at (.*) \((.*)\)$/;
const V8_OUTER2 = /^\s*at()() (\S+)$/;
const V8_INNER = /^\(?([^\(]+):(\d+):(\d+)\)?$/;

function parseV8Line(line) {
    const outer = line.match(V8_OUTER1) || line.match(V8_OUTER2);
    if (!outer) return null;
    const inner = outer[3].match(V8_INNER);
    if (!inner) return null;

    const method = outer[2] || '';
    if (outer[1]) method = `eval at ${ method}`;
    return {
        method,
        location: inner[1] || '',
        line: parseInt(inner[2]) || 0,
        column: parseInt(inner[3]) || 0
    };
}

let max = 0;

// Colorize an escape sequence for console in HTML.
function colorize(str, colorMap = defaultColorMap) {
    // eslint-disable-next-line arrow-body-style
    return (`${str }\n`).replace(regEx, (_, color, text) => {
        let colorStr = colorMap[color % colorMap.length];
        if (extraNodeRegEx.test(text)) colorStr = 'node';
        const line = parseV8Line(text);
        if (!line) return `<span class="${colorStr}">${ text }<br /></span>`;
        const loc = `${ line.location }:${ line.line }:${ line.column }`;
        if (loc.length > max) max = loc.length;
        return `<span class="${colorStr}">  at ${ escapeHTML(loc) }${ ' '.repeat(max - loc.length + 1) } ${escapeHTML(line.method)}<br /></span>`;
    });
}

colorize.regEx = regEx;

module.exports = colorize;
